# California Housing

## Supervised Learning

* Cleaning
* Data Visualisation
* Attribute Combinations
* Imputer
* oneHot Encoding & OrdinalEncoder()
* Custom Transformers
* Feature Scaling
* Transformation Pipelines
* columns transformer

## set up virtual env

### Prerequisites

* Python installed on your system. You can download Python from [python.org](https://www.python.org/downloads/).

## Steps

1. **Create a virtual environment:** Inside your project directory, create a virtual environment using the following command:

    ```bash
    python3 -m venv venv
    ```

2. **Activate the virtual environment:** Before you can start using the virtual environment, you need to activate it.

    * On macOS and Linux:

        ```bash
        source venv/bin/activate
        ```

    * On Windows:

        ```bash
        venv\Scripts\activate
        ```

3. **Install dependencies:** Now that your virtual environment is active, you can install project dependencies using `pip`. For example:

    ```bash
    python3 -m pip install -U jupyter matplotlib numpy pandas scipy scikit-learn
    ```

4. **Run jupyter notebook:**

    ```bash
    jupyter notebook
    ```
